#ifndef __ARDUINO_H__
#define __ARDUINO_H__

#include <array>
#include <boost/asio/io_service.hpp>
#include <boost/asio/serial_port.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/steady_timer.hpp>
#include <chrono>
#include <sstream>
#include <termios.h>

#include "listener.h"


namespace asio = boost::asio;


#define ARDUINO_RESET_TIMEOUT     2
#define ARDUINO_PACKAGE_BYTES     5
#define ARDUINO_TRIGGER_INTERVAL 20


struct Reading {
    int v1;
    int v2;
    void decode(const std::string& encodedLine);
};


class Arduino
{

  public:
    Arduino (const std::string& port, const int& baud=115200);
    virtual ~Arduino ();

    void start(int readings=0);

    void register_observer(std::shared_ptr<Listener> l) {
        m_observers.push_back(l);
    }

  private:
    asio::io_service   m_ioservice;
    asio::serial_port  m_serialport;
    asio::steady_timer m_trigger;

    asio::streambuf m_buffer;
    std::istream    m_input;

    int     m_nReadingsLeft;
    int     m_preReadings = 100;
    Reading m_reading;

    std::vector< std::shared_ptr<Listener> > m_observers;

    void notifyObservers();

    void reportError(const boost::system::error_code &ec,
                     const std::string& context);
    void read_handler(const boost::system::error_code &ec,
                      std::size_t bytes_transferred);
    void trigger_handler(const boost::system::error_code &ec);

    template<typename stdChronoType>
    void rearmTrigger(int duration);

    inline void flushSerialPort() { 
        ::tcflush(m_serialport.lowest_layer().native_handle(), TCIFLUSH);
    }

    void setReadingsLeft(int requestedReadings);
    bool hasReadingsLeft() const { return (m_nReadingsLeft != 0); }
};


#endif /* __ARDUINO_H__ */

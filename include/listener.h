#ifndef __LISTENER_H__
#define __LISTENER_H__


#include <iostream>
#include <memory>

#include "TCanvas.h"
#include "TH1I.h"
#include "TGraph.h"
#include "TTree.h"


class Listener {
  public:
    virtual void update(int v1, int v2) = 0;
};


class StdOutListener : public Listener {
  public:
    void update(int v1, int v2) override {
        std::cout << v1 << "\t" << v2 << std::endl;
    }
};


class RootListener : public Listener {
    public:
      virtual void write() = 0;
};


class HistListener : public RootListener {
  public:
    HistListener();
    void update(int v1, int v2) override;
    void write() override;

  private:
    std::shared_ptr<TCanvas> m_canvas;
    std::shared_ptr<TH1I> m_h1, m_h2;
};


class GraphListener : public RootListener {
  public:
    GraphListener();
    void update(int v1, int v2) override;
    void write() override;

  private:
    std::shared_ptr<TCanvas> m_canvas;
    std::shared_ptr<TGraph> m_g1, m_g2;

    void addValue(const std::shared_ptr<TGraph>& graph, const int& value);
    bool firstPointOverwritten = false;
};


class TreeListener : public RootListener {
  public:
    TreeListener();
    void update(int v1, int v2) override;
    void write() override;

  private:
    int m_v1, m_v2;
    std::shared_ptr<TTree> m_tree;
};


#endif /* __LISTENER_H__ */

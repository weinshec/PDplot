/*
    arduino.ino

    Continiously read two analog pins against the internal 1.1V reference and
    send the result over the serial connection.

    Author: Christoph Weinsheimer
    Email : christoph.weinsheimer@desy.de
*/


int analog0 = 0;
int analog1 = 1;


/**
 * writeAnalog
 *    send value as 2 byte word with left padding, e.g.
 *
 *        |00000011|11111111| = 1023
 */
void writeAnalog(int value)
{
    Serial.write(value >> 8);
    Serial.write(value & 0xFF);
}


void setup()
{
    // select internal 1.1 volt reference
    analogReference(INTERNAL);

    // Make it fast
    Serial.begin(115200);
}


void loop()
{
    writeAnalog(analogRead(analog0));
    writeAnalog(analogRead(analog1));
    Serial.write('\n');
    Serial.flush();
    delay(20);
}

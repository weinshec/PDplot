#include "listener.h"


HistListener::HistListener() :
    m_canvas(new TCanvas("HistListener", "HistListener", 1000, 800)),
    m_h1(new TH1I("h_CH1","CH1", 512, 0, 1024)),
    m_h2(new TH1I("h_CH2","CH2", 512, 0, 1024))
{
    m_canvas->Divide(1,2);
    m_canvas->cd(1);
    m_h1->Draw();
    m_canvas->cd(2);
    m_h2->Draw();
}


void
HistListener::update(int v1, int v2)
{
    m_h1->Fill(v1);
    m_h2->Fill(v2);
    m_canvas->GetPad(1)->Modified();
    m_canvas->GetPad(1)->Update();
    m_canvas->GetPad(2)->Modified();
    m_canvas->GetPad(2)->Update();
}


void
HistListener::write()
{
    m_h1->Write();
    m_h2->Write();
}


GraphListener::GraphListener() :
    m_canvas(new TCanvas("GraphListener", "GraphListener", 1000, 800)),
    m_g1(new TGraph(1)),
    m_g2(new TGraph(1))
{
    m_g1->SetName("g_CH1"); m_g1->SetTitle("CH1");
    m_g2->SetName("g_CH2"); m_g2->SetTitle("CH2");
    m_canvas->Divide(1,2);
    m_canvas->cd(1);
    m_g1->Draw("AL");
    m_canvas->cd(2);
    m_g2->Draw("AL");
}


void
GraphListener::update(int v1, int v2)
{
    addValue(m_g1, v1);
    addValue(m_g2, v2);
    firstPointOverwritten = true;
    m_canvas->GetPad(1)->Modified();
    m_canvas->GetPad(1)->Update();
    m_canvas->GetPad(2)->Modified();
    m_canvas->GetPad(2)->Update();
}


void
GraphListener::write()
{
    m_g1->Write();
    m_g2->Write();
}


void
GraphListener::addValue(const std::shared_ptr<TGraph>& graph, const int& value)
{
    if (!firstPointOverwritten)
        graph->SetPoint( 0, 0, value );
    else {
        auto N = graph->GetN();
        graph->Set(N+1);
        graph->SetPoint( N, N, value );
        graph->GetXaxis()->SetLimits(N-1000,N);
    }
}


TreeListener::TreeListener() :
    m_tree(new TTree("dataTree","dataTree"))
{
    m_tree->Branch("CH1", &m_v1);
    m_tree->Branch("CH2", &m_v2);
}


void TreeListener::update(int v1, int v2)
{
    m_v1 = v1;
    m_v2 = v2;
    m_tree->Fill();
}


void
TreeListener::write()
{
    m_tree->Write();
}



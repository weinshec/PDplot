#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/asio/read_until.hpp>

#include "arduino.h"


Arduino::Arduino(const std::string& port, const int& baud) :
    m_ioservice(),
    m_serialport(m_ioservice, port),
    m_trigger(m_ioservice),
    m_buffer(),
    m_input(&m_buffer),
    m_nReadingsLeft(0)
{
    m_serialport.set_option(asio::serial_port_base::baud_rate(baud));
}


Arduino::~Arduino()
{
    m_serialport.close();
}


void
Arduino::reportError(const boost::system::error_code &ec,
                     const std::string& context)
{
    std::cerr << "[Error] <" << context << ">   "
              << ec.message() << std::endl;
}


void
Arduino::read_handler(const boost::system::error_code &ec,
                      std::size_t bytes_transferred)
{
    if (!ec) {
        std::string line;
        std::getline(m_input, line);
        if (bytes_transferred == ARDUINO_PACKAGE_BYTES) {
            m_reading.decode(line);
            notifyObservers();
        }
    } // else reportError(ec, "Arduino::read_handler");

    flushSerialPort();

    if (hasReadingsLeft())
        rearmTrigger<std::chrono::milliseconds>(ARDUINO_TRIGGER_INTERVAL);
    else
        m_ioservice.stop();
}


void
Arduino::trigger_handler(const boost::system::error_code &ec)
{
    if (!ec) {
        asio::async_read_until(m_serialport, m_buffer, '\n',
            boost::bind( &Arduino::read_handler,
                         this,
                         boost::asio::placeholders::error,
                         boost::asio::placeholders::bytes_transferred));
    } else reportError(ec, "Arduino::trigger_handler");
}


template<typename stdChronoType> void
Arduino::rearmTrigger(int duration)
{
    m_trigger.expires_from_now( stdChronoType(duration) );
    m_trigger.async_wait(
        boost::bind( &Arduino::trigger_handler,
                     this, boost::asio::placeholders::error) );
}


void
Arduino::start(int readings)
{
    if (m_serialport.is_open()) {
        std::cout << "Arduino::start: Wait 5s for arduino to restart"
                  << std::endl;
        rearmTrigger<std::chrono::seconds>(ARDUINO_RESET_TIMEOUT);
    }

    setReadingsLeft(readings);
    m_ioservice.run();
}


void
Arduino::setReadingsLeft(int requestedReadings)
{
    if (requestedReadings <= 0)
        m_nReadingsLeft = -1;     // continous mode
    else
        m_nReadingsLeft = requestedReadings;
}


void
Arduino::notifyObservers()
{
    if (m_preReadings > 0)
        m_preReadings--;
    else {
        m_nReadingsLeft--;
        for (const auto& o : m_observers)
            o->update(m_reading.v1, m_reading.v2);
    }
}


void
Reading::decode(const std::string& encodedLine)
{
    v1 = (encodedLine[0] << 8) + (encodedLine[1] & 0xFF);
    v2 = (encodedLine[2] << 8) + (encodedLine[3] & 0xFF);
}

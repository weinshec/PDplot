#include <iostream>
#include <thread>
#include <boost/program_options.hpp>

#include <TApplication.h>
#include <TFile.h>

#include "arduino.h"
#include "listener.h"


namespace po = boost::program_options;

using SPtrListener     = std::shared_ptr<Listener>;
using SPtrRootListener = std::shared_ptr<RootListener>;


bool rootAppNeeded(const po::variables_map& vm);


int main(int argc, char *argv[]) {

    Arduino a("/dev/ttyACM0");
    std::vector<SPtrListener> listeners;
    int nReadings = 0;

    std::vector<SPtrRootListener> rootListeners;
    std::unique_ptr<TApplication> rootapp;
    std::unique_ptr<std::thread>  rootappThread;
    std::string                   rootFileName;
    std::unique_ptr<TFile>        rootFile;


    po::options_description desc("Allowed options");
    desc.add_options()
        ("help",     "produce help message")
        (",n",       po::value<int>(&nReadings)->default_value(100),
                     "stop after n readings (0 for continuous mode)")
        ("stdout,s", "dump values on stdout")
        ("hist,h",   "show realtime histogram of channels")
        ("graph,g",  "show realtime graph of channels")
        ("file,f",   po::value<std::string>(&rootFileName),
                     "output root file (not to be used with -n 0)")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 1;
    }

    if (vm.count("stdout"))
        listeners.push_back(SPtrListener(new StdOutListener));

    if (rootAppNeeded(vm))
        rootapp = std::unique_ptr<TApplication>(
                new TApplication("PDplot", 0, {}));

    if (vm.count("hist"))
        rootListeners.push_back(SPtrRootListener(new HistListener));
    if (vm.count("graph"))
        rootListeners.push_back(SPtrRootListener(new GraphListener));
    if (vm.count("file")) {
        if (nReadings == 0) {
            std::cout << std::endl
                      << "\tCannot save to root file while in continuous mode!"
                      << std::endl << std::endl << desc << std::endl;
            return 1;
        }
        rootListeners.push_back(SPtrRootListener(new TreeListener));
        rootFile = std::unique_ptr<TFile>(
                new TFile(rootFileName.c_str(), "RECREATE"));
    }

    if (listeners.size() == 0 && rootListeners.size() == 0) {
        std::cout << desc << std::endl;
        return 1;
    }

    for (const auto& listener : listeners)
        a.register_observer(listener);
    for (const auto& listener : rootListeners)
        a.register_observer(listener);

    if (rootapp)
        rootappThread = std::unique_ptr<std::thread>(
            new std::thread(&TApplication::Run, &*rootapp, false));

    a.start(nReadings);

    if (rootapp) {
        if (rootFile) {
            for (const auto& listener : rootListeners)
                listener->write();
            rootFile->Close();
        }
        std::cout << "TApplication still running... Press <CTRL>+C to quit!"
                  << std::endl;
        rootappThread->join();
    }

    return 0;
}



bool rootAppNeeded(const po::variables_map& vm) {
    return vm.count("hist") || vm.count("graph") || vm.count("file");
}

CMAKE_MINIMUM_REQUIRED(VERSION 2.6 FATAL_ERROR)

PROJECT(PDplot)

ADD_DEFINITIONS("-std=c++11 -Wall -Wextra -Wno-long-long")

find_package(Boost COMPONENTS system program_options REQUIRED)

list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})
find_package(ROOT REQUIRED COMPONENTS Core RIO Net Hist Graf Gpad Tree MathCore)
include(${ROOT_USE_FILE})

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)

add_executable( ${PROJECT_NAME} src/main.cc src/arduino.cc src/listener.cc)
target_link_libraries( ${PROJECT_NAME}
                       ${Boost_LIBRARIES}
                       ${ROOT_LIBRARIES})
